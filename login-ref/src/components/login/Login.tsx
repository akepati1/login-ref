import React, { useEffect, useRef, useState } from "react";
import Input from "../input/Input";
import "./login.css";
function Login() {
  const [userName, setUserName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  let inputRef = useRef<HTMLInputElement>(null);
  let passwordRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    inputRef?.current?.focus();
  }, []);
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name == "username") {
      setUserName(e.target.value);
    } else if (e.target.name == "password") {
      setPassword(e.target.value);
    }
  };
  const handleLogin = (e: any) => {
    e.preventDefault();
    console.log(
      `usernName ${inputRef?.current?.value} password ${passwordRef?.current?.value}`
    );
  };
  return (
    <div className="container">
      <div className="title">
        <h1>LOGIN</h1>
      </div>
      <div className="form">
        <form>
          <Input
            name="username"
            value={userName}
            type="text"
            inputRef={inputRef}
            handleChange={onChange}
            placeHolder="USER_NAME"
          />
          <Input
            name="password"
            value={password}
            type="password"
            inputRef={passwordRef}
            handleChange={onChange}
            placeHolder="PASSWORD"
          />
          <div className="details">
            <div className="remember">
              <input type="checkbox" name="remember" />
              <p>Remember_me</p>
            </div>
            <div className="forgot">
              <p>Forgot?</p>
            </div>
          </div>
          <div className="button_container">
            <button
              onClick={(e) => {
                handleLogin(e);
              }}
              className="login_button"
            >
              Login
            </button>
          </div>
        </form>
      </div>
      <div className="submit"></div>
    </div>
  );
}

export default Login;
