import React from "react";
import "./input.css";
import {inputPropType} from "../types/inputTypes";
function Input(props: inputPropType) {
  const { type, inputRef, name, handleChange, value,placeHolder } = props;
  return (
    <div className="input-container">
      <input className="input"
        name={name}
        type={type}
        value={value}
        ref={inputRef}
        placeholder={placeHolder}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
}

export default Input;
