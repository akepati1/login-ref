import React from "react";
export type inputPropType = {
    type: string;
    name: string;
    value: string;
    placeHolder: string;
    inputRef: React.RefObject<HTMLInputElement>;
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  };